# SquaredJS - Platformer Example

This is an example project for [SquaredJS](https://squaredjs.com/) - Live demo [here](http://squaredjs.com/demos/platformer/)

## How to launch

```
npm install
npm start
```