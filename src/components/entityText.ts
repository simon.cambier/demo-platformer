import { Component, UILabel, Entity } from 'squaredjs'
const debug = require('debug')('game:components:playerName')

export default class EntityText extends Component {

  public get text(): string {
    return this.label.text
  }
  public set text(v: string) {
    this.label.text = v
    this.label.setStyle({})
  }

  public label: UILabel = new UILabel()

  public constructor(entity: Entity, text: string) {
    super(entity)
    this.text = text
    this.label.setStyle({ fontSize: 32, strokeThickness: 2, align: 'center' })
    this.entity.sprite.parent.addChild(this.label)
  }

  public destroy(): void {
    this.label.destroy()
  }

  public update(delta: number): void {
    this.label.x = this.entity.x + this.entity.width / 2
    this.label.y = this.entity.y - 32
  }
}
